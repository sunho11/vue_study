### Vue Router

1. Vue Router

   - routing : 웹 페이지 간의 이동 방법
   - Vue.js 공식 라우터
   - 라우트에 컴포넌트를 매핑한 후, 어떤 주소에 렌더링할 지 알려줌
   - SPA 상에서 라우팅을 쉽게 개발할 수 있는 기능을 제공
   - URL에 따라 컴포넌트를 연결하고 설정된 컴포넌트를 보여줌

2. 설치

   - CDN 방식 : script src 방식

   - NPM 방식 

     ```
     npm install vue-router
     ```

   - Vue Cli

     ```
     vue add router
     ```

     - 이 경우, history mode 를 사용한다

3. index.js

   - 라우트에 관련된 정보/설정

   ```javascript
   import Vue from 'vue'
   import VueRouter from 'vue-router'
   import HomeView from '../views/HomeView.vue'
   import AboutView from '../views/AboutView.vue'
   import UserView from '../views/UserView.vue'
   import BoardView from '../views/BoardView.vue'
   import BoardList from '../components/board/BoardList.vue'
   import BoardWrite from '../components/board/BoardWrite.vue'
   
   Vue.use(VueRouter)
   
   const routes = [
     {
       path: '/',
       name: 'home',
       component: HomeView
     },
     {
       path: '/about',
       name: 'about',
       component: AboutView
     }
   ]
   
   const router = new VueRouter({
     mode:'history',
     base: process.env.BASE_URL,
     routes
   })
   
   export default router
   ```
   
4. < router-link >

   - 사용자 네비게이션을 가능하게 하는 컴포넌트
   - 목표 위치는 to prop으로 지정됨
   - 기본적으로는 올바른 href를 갖는 a태그로 렌더링된다

   ```javascript
   <router-link to="/">Home</router-link> 
   ```

   - 여기의 to는 index.js의 url을 넣어주면 된다.

5. < router-view >

   - 주어진 라우트에 대해 일치하는 컴포넌트를 렌더링하는 함수형 컴포넌트
   - 실제 component가 DOM에 부착되어 보이는 자리
   - router-link를 클릭하면 해당 경로와 연결되어 있는 index.js에 정의한 컴포넌트가 위치

6. History Mode

   - vue-router의 기본모드는 hash mode(URL에 #이 붙어서 URL이 변경될 때 페이지가 다시 로드되는 걸 방지)
   - 해시를 제거하기 위해 HTML History APi를 이용하여 router를 구현

7. Named Routes

   - 라우트에 이름을 명명할 수 있다

   ```javascript
   <router-link :to="{name:'about'}">About</router-link>
   ```

   - 여기의 name이 index.js의 name이다!

8. 프로그래밍 방식 네비게이션

   - router-link 없이도 router의 인스턴스 메소드를 이용하여 수행 가능

   - $router로 접근가능

   - this.$router.push를 호출해서 사용 가능(뒤로 가기도 가능. 히스토리 스택이기 때문)

   - 방식

     ```javascript
     //리터럴 string
     router.push('home')
     
     //object
     router.push({path : 'home'})
     
     //named Route
     router.push({name : 'user', params:{userId:123}})
     
     //쿼리와 사용          결과는 /register?plan=private
     router.push({path:'register',query:{plan:'private'}}) 
     ```

9. 동적 라우트 매칭

   - 주어진 패턴을 가진 라우트를 동일한 컴포넌트에 매핑해야하는 경우
   - 동적인자 전달
   
   ```javascript
   {
       path:'/user/:username',
       name:'user',
       component:UserView
     },
   ```
   
   - 이처럼 앞에 :을 붙여주면 된다
   - 이름으로도 접근 가능
   
   ```javascript
   <router-link :to="{name:'user',params:{username:'Lee'}}">
   ```
   
   - 그러면 user에서 params를 사용해야 한다.
   
   ```javascript
   {{$route.params.username}}
   ```
   
   - 그런데 이 경우, 이동하면 이 페이지 자체에서 갈아끼워지게 된다. 
   - 만약, 이 페이지 내의 한 부분에서 사용하고 싶다면 (내부에서 router-view를 사용하고 싶다면)아래를 보자!
   
10. 중첩된 라우트

    - 실제 앱 UI는 일반적으로 여러 단계로 중첩된 컴포넌트 구조
    - URL의 세그먼트가 중첩된 컴포넌트의 특정 구조와 일치하는 것을 활용

    ```javascript
    {
        path:'/board',
        name:'board',
        component:BoardView,
        children:[
          {
            path:'list',
            component:BoardList
          },
          {
            path:'write',
            component:BoardWrite
          }
        ]
      }
    ```

    - 이 경우, board/list 또는 board/write를 사용하여 url을 매핑한다!

11. components와 views

    - views : App.vue를 제외한 최상위 View
    - components : views 하위의 component를 전부 모아둠

